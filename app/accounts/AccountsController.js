(function () {
    'use strict';

    angular
        .module('bullsfirst')
        .controller('AccountsCtrl', Ctrl);

    function Ctrl(AccountsService) {
        var vm = this;
        vm.rows = AccountsService.accounts();
        vm.totals = AccountsService.totals();
        vm.addAccount = addAccount;

        function addAccount(){
            var newAccount = {
                name: 'New Account',
                marketValue: Math.random() * 100000,
                cash: Math.random() * 400000,
                legend: 'cyan'
            };

            AccountsService.addAccount(newAccount);
        }
    }
}());